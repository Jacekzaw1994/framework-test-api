'use strict';

const Hapi = require('hapi');

const hugeData = require("./data/10k-rows-data");
const data = require("./data/1k-rows-data");

const start = async () => {

    const server = Hapi.server({
        port: 8000,
        host: 'localhost',
        routes: {
            cors: true
        }
    });

    server.route({
        method: 'GET',
        path:'/api/10k-rows',
        handler: (r, h) => hugeData
    });

    server.route({
        method: 'GET',
        path:'/api/1k-rows',
        handler: (r, h) => data
    });

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

start();